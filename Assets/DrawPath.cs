﻿using UnityEngine;
using System.Collections;

public class DrawPath : MonoBehaviour 
{
    const int ITERATIONS = 65536;

	void OnDrawGizmosSelected()
    {
        int quad1 = ITERATIONS / 4;
        int quad2 = ITERATIONS / 2;
        int quad3 = quad1 + quad2;
        float radius;

        Vector3 initPos = transform.position;

        if(gameObject.tag.Equals("Planet"))
            radius = gameObject.GetComponent<Planet_Simple>().GetRadius();
        else
            radius = (transform.parent.position - transform.position).magnitude;

        Gizmos.color = Color.yellow;

        for (int i = 0; i < ITERATIONS; i++)
        {
            float newX = radius * Mathf.Cos(RadToDeg(i / ITERATIONS));
            float newZ = radius * Mathf.Sin(RadToDeg(i / ITERATIONS));
            Vector3 newPos = new Vector3(newX, 0, newZ);
            Gizmos.DrawLine(initPos, newPos);
            initPos = newPos;          
        }
        
	}

    float RadToDeg(float rad)
    {
        return rad * 180 / Mathf.PI;
    }
}
