﻿using UnityEngine;
using System.Collections;

/**
 * Moons are slightly different in that, since they're children to planets,
 *    they operate on local positioning. Use this script for moons.
 */
public class Moon_Simple : MonoBehaviour 
{
    [SerializeField]
    GameObject orbitBody;

    public float mass;
    [Range(0.1f, 0.5f)]
    public float diameter;
    public float rotationSpeed;

    [Range(1, 2)]
    public float surfaceDistance;
    public float orbitSpeed;

    public bool simplify;


	// Use this for initialization
	void Start ()
    {
        InitializeDiameter();
        InitializePosition();
	}
	
	// Update is called once per frame
	void Update () 
    {
        Rotate();
        Orbit();
    }

    void InitializeDiameter()
    {
        Vector3 size = new Vector3(diameter, diameter, diameter);
        transform.localScale = size;
    }

    void InitializePosition()
    {
        if (simplify)
        {
            transform.localPosition = new Vector3(0, 0, 1);
        }
        else
        {
            float initX, initY, initZ;
            float initAngle = Random.Range(0, 360);

            initX = surfaceDistance * Mathf.Cos(initAngle);
            initY = 0;
            initZ = surfaceDistance * Mathf.Sin(initAngle);

            Vector3 initPos = new Vector3(initX, initY, initZ);
            transform.localPosition = initPos;
        }
    }

    void Rotate()
    {
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }

    void Orbit()
    {
        transform.RotateAround(orbitBody.transform.position, Vector3.up, orbitSpeed * Time.deltaTime);
    }
}
